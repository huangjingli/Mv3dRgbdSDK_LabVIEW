#include "Mv3dRgbdApi.h"

#include <string.h>
#include <malloc.h>


//#ifdef __cplusplus
//extern "C" {
//
//

typedef struct _MV3D_RGBD_GigEDEVICE_INFO_
{
	char                        chManufacturerName[32];             // 设备厂商
	char                        chModelName[32];                    // 设备型号
	char                        chDeviceVersion[32];                // 设备版本
	char                        chManufacturerSpecificInfo[48];     // 设备厂商特殊信息
	char                        chSerialNumber[16];                 // 设备序列号
	char                        chUserDefinedName[16];              // 设备用户自定义名称
	int32_t          enDeviceType;                       // 设备类型：网口、USB
	unsigned char               chMacAddress[8];                    // Mac地址
	int32_t           enIPCfgMode;                        // 当前IP类型
	char                        chCurrentIp[16];                    // 设备当前IP
	char                        chCurrentSubNetMask[16];            // 设备当前子网掩码
	char                        chDefultGateWay[16];                // 设备默认网关
	char                        chNetExport[16];                    // 网口IP地址
	uint8_t                     nReserved[16];                      // 保留字节
} MV3D_RGBD_GigEDEVICE_INFO;

typedef struct _MV3D_RGBD_U3DEVICE_INFO_
{
	char                        chManufacturerName[32];             // 设备厂商
	char                        chModelName[32];                    // 设备型号
	char                        chDeviceVersion[32];                // 设备版本
	char                        chManufacturerSpecificInfo[48];     // 设备厂商特殊信息
	char                        chSerialNumber[16];                 // 设备序列号
	char                        chUserDefinedName[16];              // 设备用户自定义名称
	int32_t          enDeviceType;                       // 设备类型：网口、USB
	uint32_t                    nVendorId;                          // 供应商ID号 
	uint32_t                    nProductId;                         // 产品ID号 
	int32_t         enUsbProtocol;                      // 支持的USB协议
	char                        chDeviceGUID[64];                   // 设备GUID号
	uint8_t                     nReserved[16];                      // 保留字节 
} MV3D_RGBD_U3DEVICE_INFO;



/************************************************************************
*  @fn     MV3D_RGBD_GetGigEDeviceList()
*  @brief  获取GigE设备列表
*  @param  nDeviceType                 [IN]            设备类型,见Mv3dRgbdDeviceType,可全部获取(DeviceType_Ethernet | DeviceType_USB)
*  @param  pstDeviceInfos              [IN OUT]        设备列表
*  @param  nMaxDeviceCount             [IN]            设备列表缓存最大个数
*  @param  pDeviceCount                [OUT]           填充列表中设备个数
*  @return 成功，返回MV3D_RGBD_OK；错误，返回错误码

*  @fn     MV3D_RGBD_GetGigEDeviceList()
*  @brief  Gets GigE 3D cameras list
*  @param  nDeviceType                 [IN]            device type，refer to Mv3dRgbdDeviceType，get all(DeviceType_Ethernet | DeviceType_USB)
*  @param  pstDeviceInfos              [IN OUT]        devices list
*  @param  nMaxDeviceCount             [IN]            Max Number of device list caches
*  @param  pDeviceCount                [OUT]           number of devices in the fill list
*  @return Success, return MV3D_RGBD_OK. Failure, return error code
************************************************************************/
MV3D_RGBD_STATUS __stdcall  MV3D_RGBD_GetGigEDeviceList(MV3D_RGBD_GigEDEVICE_INFO* pstGigEDeviceInfos, uint32_t nMaxDeviceCount, uint32_t* pDeviceCount);

/************************************************************************
*  @fn     MV3D_RGBD_GetU3DeviceList()
*  @brief  获取USB设备列表
*  @param  nDeviceType                 [IN]            设备类型,见Mv3dRgbdDeviceType,可全部获取(DeviceType_Ethernet | DeviceType_USB)
*  @param  pstDeviceInfos              [IN OUT]        设备列表
*  @param  nMaxDeviceCount             [IN]            设备列表缓存最大个数
*  @param  pDeviceCount                [OUT]           填充列表中设备个数
*  @return 成功，返回MV3D_RGBD_OK；错误，返回错误码

*  @fn     MV3D_RGBD_GetU3DeviceList()
*  @brief  Gets USB 3D cameras list
*  @param  nDeviceType                 [IN]            device type，refer to Mv3dRgbdDeviceType，get all(DeviceType_Ethernet | DeviceType_USB)
*  @param  pstDeviceInfos              [IN OUT]        devices list
*  @param  nMaxDeviceCount             [IN]            Max Number of device list caches
*  @param  pDeviceCount                [OUT]           number of devices in the fill list
*  @return Success, return MV3D_RGBD_OK. Failure, return error code
************************************************************************/
MV3D_RGBD_STATUS  __stdcall MV3D_RGBD_GetU3DeviceList(MV3D_RGBD_U3DEVICE_INFO* pstU3DeviceInfos, uint32_t nMaxDeviceCount, uint32_t* pDeviceCount);

/************************************************************************
*  @fn     MV3D_RGBD_OpenGigEDevice()
*  @brief  打开GigE设备
*  @param  handle                      [IN OUT]        相机句柄
*  @param  pstDeviceInfo               [IN]            枚举的设备信息，默认为空，打开第一个相机
*  @return 成功，返回MV3D_RGBD_OK；错误，返回错误码

*  @fn     MV3D_RGBD_OpenDevice()
*  @brief  open GigE device
*  @param  handle                      [IN OUT]        camera handle
*  @param  pstDeviceInfo               [IN]            enum camera info. the default is null, open first camera
*  @return Success, return MV3D_RGBD_OK. Failure, return error code
************************************************************************/
MV3D_RGBD_STATUS  __stdcall MV3D_RGBD_OpenGigEDevice(HANDLE *handle, MV3D_RGBD_GigEDEVICE_INFO* pstGigeDeviceInfo = NULL);

/************************************************************************
*  @fn     MV3D_RGBD_OpenU3Device()
*  @brief  打开USB设备
*  @param  handle                      [IN OUT]        相机句柄
*  @param  pstDeviceInfo               [IN]            枚举的设备信息，默认为空，打开第一个相机
*  @return 成功，返回MV3D_RGBD_OK；错误，返回错误码

*  @fn     MV3D_RGBD_OpenDevice()
*  @brief  open USB device
*  @param  handle                      [IN OUT]        camera handle
*  @param  pstDeviceInfo               [IN]            enum camera info. the default is null, open first camera
*  @return Success, return MV3D_RGBD_OK. Failure, return error code
************************************************************************/
MV3D_RGBD_STATUS __stdcall  MV3D_RGBD_OpenU3Device(HANDLE *handle, MV3D_RGBD_U3DEVICE_INFO* pstU3DeviceInfo = NULL);

/************************************************************************
*  @fn     MV3D_RGBD_GetDeviceInfo
*  @brief  获取当前设备的详细信息
*  @param  handle                      [IN]            相机句柄
*  @param  pstDevInfo                  [IN][OUT]       返回给调用者有关相机设备信息结构体指针
*  @return 成功,MV3D_RGBD_OK,失败,返回错误码

*  @fn     MV3D_RGBD_GetDeviceInfo
*  @brief  Get current device information
*  @param  handle                      [IN]            camera handle
*  @param  pstDevInfo                  [IN][OUT]       Structure pointer of device information
*  @return Success, return MV3D_RGBD_OK. Failure, return error code
************************************************************************/
MV3D_RGBD_STATUS __stdcall  MV3D_RGBD_GetU3DeviceInfo(HANDLE handle, MV3D_RGBD_U3DEVICE_INFO* pstDevInfo);

/************************************************************************
*  @fn     MV3D_RGBD_GetDeviceInfo
*  @brief  获取当前设备的详细信息
*  @param  handle                      [IN]            相机句柄
*  @param  pstDevInfo                  [IN][OUT]       返回给调用者有关相机设备信息结构体指针
*  @return 成功,MV3D_RGBD_OK,失败,返回错误码

*  @fn     MV3D_RGBD_GetDeviceInfo
*  @brief  Get current device information
*  @param  handle                      [IN]            camera handle
*  @param  pstDevInfo                  [IN][OUT]       Structure pointer of device information
*  @return Success, return MV3D_RGBD_OK. Failure, return error code
************************************************************************/
MV3D_RGBD_STATUS __stdcall  MV3D_RGBD_GetGigEDeviceInfo(HANDLE handle, MV3D_RGBD_GigEDEVICE_INFO* pstDevInfo);


/************************************************************************
*  @fn     MV3D_RGBD_YUV422ToRGB
*  @brief  将YUV422转换为RGB
*  @param  pYuvImgge                  [IN][OUT]       输入的YUV422数据
*  @param  pRgbImage                  [IN][OUT]       输出的RGB数据
*  @return 成功,MV3D_RGBD_OK,失败,返回错误码，不支持的格式

*  @fn     MV3D_RGBD_GetDeviceInfo
*  @brief  yuv422 image convert to rgb image
*  @param  pYuvImgge                  [IN][OUT]       In YUV422 data
*  @param  pRgbImage                  [IN][OUT]       Out RGB data
*  @return Success, return MV3D_RGBD_OK. Failure, return error code
************************************************************************/
MV3D_RGBD_STATUS __stdcall MV3D_RGBD_YUV422ToRGB(MV3D_RGBD_IMAGE_DATA *pYuvImgge, MV3D_RGBD_IMAGE_DATA* pRgbImage);
//}
//#endif
